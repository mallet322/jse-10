package ru.shokin.tm.service;

import ru.shokin.tm.entity.Task;
import ru.shokin.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description);
    }

    public Task update(final Long id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public void clear() {
        taskRepository.clear();
    }

    public Task removeByIndex(final int index) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task removeById(final Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    public Task findByIndex(int index) {
        if (index < 0 || index > taskRepository.findAll().size() - 1) return null;
        return taskRepository.findByIndex(index);
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task findByProjectIdAndId(Long projectId, Long id) {
        if (projectId == null || id == null) return null;
        return taskRepository.findByProjectIdAndId(projectId, id);
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

}